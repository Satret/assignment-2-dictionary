%include "words.inc"
%include "lib.inc"

%define buffer_size 255

global _start

extern find_word

section .rodata
    buffer_OF_message: db "Buffer overflow", 0
    key_error_message: db "Key is not found", 0

section .bss
    buffer: resb buffer_size

section .text

_start:
    mov rdi, buffer
    mov rsi, buffer_size
    call read_word

    cmp rax,0
    je .buffer_OF_error

    mov rdi, rax
    mov rsi, next_element
    push rdx
    call find_word
    cmp rax, 0
	je .key_error

    mov rdi, rax
    add rdi, 8
    inc rdi
    add rdi, rdx
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

    .buffer_OF_error:
        mov rdi, buffer_OF_message
        jmp .err_exit

    .key_error:
        mov rdi, key_error_message

    .err_exit:
        call print_error
        call print_newline
        mov rdi, 1
        call exit
