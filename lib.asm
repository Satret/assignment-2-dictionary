section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax

    .loop:
        cmp byte[rax+rdi],0
        je .end
        inc rax
        jmp .loop

    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov r8, rax ;длина строки
    pop rdi
    mov r9, rdi ;адрес начала строки
    mov rcx, 0

    .loop:
        cmp r8, rcx
        je .end
        mov rax, 1
        mov rdi, 1
        mov rsi, r9
        mov rdx, 1
        push rcx
        syscall
        pop rcx
        add rcx, 1
        inc r9
        jmp .loop

    .end:
        ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push 0xA
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r8, rsp
    mov r9, 10
    push 0

    .loop:
        mov rdx, 0
        div r9
        add rdx, 48
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        ja .loop
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        mov rsp, r8
        ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    	xor rax, rax
	cmp rdi, 0
	jl .neg
	call print_uint
	ret

	.neg:
	push rdi
	mov rdi, 45
	call print_char
	pop rdi
	neg rdi
	call print_uint
	xor rax, rax
   	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r8b, [rdi];адрес 1 числа
        mov r9b, [rsi];адрес 2 числа
        cmp r8b, r9b
        jne .out0
        cmp r8b, 0
        je .out1
        inc rdi
        inc rsi
        jmp .loop

    .out1:
        mov rax, 1
        ret

    .out0:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    xor rcx, rcx

    .loop:
        push rdi;   аддрес
        push rsi;   размер
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, 0
        jz .exit1
        cmp rcx, 0
        jnz  .middle

        .first:
            cmp rax, 0x20
            jz  .loop
            cmp rax, 0x9
            jz  .loop
            cmp rax, 0xA
            jz  .loop

        .middle:
            cmp rax, 0x20
            jz  .exit1
            cmp rax, 0x9
            jz  .exit1
            cmp rax, 0xA
            jz  .exit1

        .length:
            cmp rcx, rsi
            jz .exit2
            mov [rdi+rcx], rax
            inc rcx
            jmp .loop

    .exit1:
        mov byte[rdi+rcx], 0
        mov rdx, rcx
        pop rax
        ret

    .exit2:
        pop rax
        mov rdx, rcx
        xor rax, rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8

    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8, 0x30
        jb .end
        cmp r8, 0x39
        ja .end
        and r8b, 0xf
        mov r9, 10
        mul r9
        add rax, r8
        inc rcx
    	jmp .loop

    .end:
    	mov rdx, rcx
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
     mov al, byte[rdi]
     cmp al, 0x2D
     je .neg
     call parse_uint
     jmp .end

     .neg:
          inc rdi
          call parse_uint
          inc rdx
          neg rax
          ret

     .end:
         ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rcx, rdx ; длина буфера
    xor rax, rax

    .loop:
        cmp rcx, 0
        jl .end1
        mov r8, [rdi] ;адрес строки
        mov r9, [rsi] ;адрес буфера
        cmp r8, 0
        je .end
        mov [rsi], r8
        inc rdi
        inc rsi
        dec rcx
        inc rax
        jmp .loop

    .end:
        ret

    .end1:
        mov rax, 0
        ret

; Выводит переданное сообщение об ошибке
print_error:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret
