ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
program: main.o dict.o lib.o
	$(LD) -o $@ $^
clean:
	rm -f *.o program

.PHONY: clean

