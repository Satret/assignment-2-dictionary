%define cell_size 8

extern string_equals

section .text

global find_word

find_word:
    xor r8, r8
    .loop:
        push rsi
        push rdi
        add rsi, cell_size
        call string_equals
        pop rdi
        pop rsi

        cmp rax, 1
        jz .success

        mov r8, [rsi]
        cmp r8, 0
        mov rsi, r8
        jnz .loop

    .not_success:
        xor rax, rax
        ret

    .success:
        mov rax, rsi
        ret
